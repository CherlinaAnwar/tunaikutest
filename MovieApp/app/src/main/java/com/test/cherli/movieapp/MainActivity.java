package com.test.cherli.movieapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.test.cherli.movieapp.adapter.DetailMovie;
import com.test.cherli.movieapp.adapter.MoviesAdapter;
import com.test.cherli.movieapp.model.MovieRespon;
import com.test.cherli.movieapp.model.movie;
import com.test.cherli.movieapp.rest.ApiClient;
import com.test.cherli.movieapp.rest.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private String[] activityTitles;
    ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private static final String TAG = MainActivity.class.getSimpleName();

    // TODO - insert your themoviedb.org API KEY here
    private final static String API_KEY = "ccbd4822fdecaf412988aee13a652508";
    public static final String EMP_ID = "id";
    public static final String TAG_TITLE="title";
    public static final String TAG_ORI_TITLE="original_title";
    public static final String TAG_OVERVIEW="overview";
    public static final String TAG_RATING="vote_average";
    public static final String TAG_PHOTO="poster_path";

    public static int navItemIndex = 0;
    private static final String TAG_HOME = "home";
    public static String CURRENT_TAG = TAG_HOME;
    private Context c;
    ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        c=getApplicationContext();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navHeader = navigationView.getHeaderView(0);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_HOME;

        }
        recyclerView = (RecyclerView) findViewById(R.id.movies_recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));

        if (API_KEY.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please obtain your API KEY first from themoviedb.org", Toast.LENGTH_LONG).show();
            return;
        }
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

//        Call<MovieRespon> call = apiService.getTopRatedMovies(API_KEY);
        Call<MovieRespon> call = apiService.getPopularity(API_KEY);
        call.enqueue(new Callback<MovieRespon>() {
            @Override
            public void onResponse(Call<MovieRespon>call, Response<MovieRespon> response) {
//                progressDialog=new ProgressDialog(MainActivity.this);
//                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//                progressDialog.setMessage("Please Wait...");
//                progressDialog.setCancelable(false);
//                progressDialog.show();

                int statusCode=response.code();
                final List<movie> movies = response.body().getResults();
                recyclerView.setAdapter(new MoviesAdapter(movies,R.layout.list_item_movie, getApplicationContext()));

                recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
                        View child = rv.findChildViewUnder(e.getX(), e.getY());
                        int position = rv.getChildAdapterPosition(child);
                        Bundle bundle=new Bundle();
                        bundle.putString(EMP_ID,movies.get(position).getId().toString());
                        bundle.putString(TAG_TITLE, movies.get(position).getTitle());
                        bundle.putString(TAG_ORI_TITLE,movies.get(position).getOriginalTitle());
                        bundle.putString(TAG_OVERVIEW, movies.get(position).getOverview());
                        bundle.putString(TAG_RATING, movies.get(position).getVoteAverage().toString());
                        bundle.putString(TAG_PHOTO, movies.get(position).getPosterPath());
                        Intent i = new Intent(getApplicationContext(),DetailMovie.class);
                        i.putExtras(bundle);
                        startActivity(i);
                        return false;
                    }

                    @Override
                    public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean b) {

                    }
                });

            }

            @Override
            public void onFailure(Call<MovieRespon>call, Throwable t) {
                // Log error here since request failed
                Log.e(TAG, t.toString());
//                progressDialog.dismiss();
            }
        });

    }

    private void setUpNavigationView() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_HOME;
                        break;
                    case R.id.nav_photos:
                        break;
                    case R.id.nav_movies:
                        navItemIndex=2;
//                        Intent intent=new Intent(Navigation_Drawer.this, MainActivity.class);
//                        startActivity(intent);
                    default:
                        navItemIndex = 2;
                }
                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                return true;
            }
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void loadNavHeader() {
        // showing dot next to notifications label
        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_populer) {
            Call<MovieRespon> call = apiService.getPopularity(API_KEY);
            call.enqueue(new Callback<MovieRespon>() {
                @Override
                public void onResponse(Call<MovieRespon>call, Response<MovieRespon> response) {
                    int statusCode=response.code();
                    List<movie> movies = response.body().getResults();
                    recyclerView.setAdapter(new MoviesAdapter(movies,R.layout.list_item_movie, getApplicationContext()));
                }

                @Override
                public void onFailure(Call<MovieRespon>call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                }
            });

            return true;
        }

        if (id == R.id.action_toprate) {
            Call<MovieRespon> call = apiService.getTopRatedMovies(API_KEY);
            call.enqueue(new Callback<MovieRespon>() {
                @Override
                public void onResponse(Call<MovieRespon>call, Response<MovieRespon> response) {
                    int statusCode=response.code();
                    List<movie> movies = response.body().getResults();
                    recyclerView.setAdapter(new MoviesAdapter(movies,R.layout.list_item_movie, getApplicationContext()));
                }

                @Override
                public void onFailure(Call<MovieRespon>call, Throwable t) {
                    // Log error here since request failed
                    Log.e(TAG, t.toString());
                }
            });
        }

        return super.onOptionsItemSelected(item);
    }
}
