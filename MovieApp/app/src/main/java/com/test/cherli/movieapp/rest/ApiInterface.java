package com.test.cherli.movieapp.rest;

import com.test.cherli.movieapp.model.MovieRespon;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("movie/popular")
    public Call<MovieRespon> getPopularity(@Query("api_key") String apiKey);

    @GET("movie/top_rated")
    public Call<MovieRespon> getTopRatedMovies(@Query("api_key") String apiKey);

    @GET("movie/{id}")
    Call<MovieRespon> getMovieDetails(@Path("id") int id, @Query("api_key") String apiKey);
}
