package com.test.cherli.movieapp.adapter;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.cherli.movieapp.R;
import com.test.cherli.movieapp.model.movie;

import java.util.List;

public class DetailMovie extends AppCompatActivity {
    CollapsingToolbarLayout collapsingToolbar;
    int mutedColor = R.attr.colorPrimary;
    TextView movieTitle;
    TextView data;
    TextView movieDescription;
    TextView rating;
    ImageView iv_photo;
    private List<movie> movies;
    public static final String EMP_ID = "id";
    public static final String TAG_TITLE="title";
    public static final String TAG_ORI_TITLE="original_title";
    public static final String TAG_OVERVIEW="overview";
    public static final String TAG_RATING="vote_average";
    public static final String TAG_PHOTO="poster_path";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);

        data=(TextView)findViewById(R.id.subtitle);
        movieTitle = (TextView)findViewById(R.id.title);
        movieDescription=(TextView)findViewById(R.id.description);
        rating=(TextView)findViewById(R.id.rating);
        iv_photo=(ImageView)findViewById(R.id.iv_photo);

        if(getIntent().getExtras()!=null){
            Bundle bundle=getIntent().getExtras();
            movieTitle.setText(bundle.getString(TAG_TITLE));
            data.setText(bundle.getString(TAG_ORI_TITLE));
            movieDescription.setText(bundle.getString(TAG_OVERVIEW));
            rating.setText(bundle.getString(TAG_RATING));
            Glide.with(this).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2" + bundle
                    .getString(TAG_PHOTO))
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(iv_photo);

        }else{
            movieTitle.setText(getIntent().getStringExtra(TAG_TITLE));
            data.setText(getIntent().getStringExtra(TAG_ORI_TITLE));
            movieDescription.setText(getIntent().getStringExtra(TAG_OVERVIEW));
            rating.setText(getIntent().getStringExtra(TAG_RATING));
            Glide.with(this).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2" + getIntent()
                    .getStringExtra(TAG_PHOTO))
                    .placeholder(R.mipmap.ic_launcher_round)
                    .into(iv_photo);
        }

    }
}
