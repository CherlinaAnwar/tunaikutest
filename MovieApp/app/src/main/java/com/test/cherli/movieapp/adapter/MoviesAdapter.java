package com.test.cherli.movieapp.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Movie;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.test.cherli.movieapp.MainActivity;
import com.test.cherli.movieapp.R;
import com.test.cherli.movieapp.model.movie;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private List<movie> movies;
    private int rowLayout;
    private Context context;

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        movie item= movies.get(position);
        holder.movieTitle.setText(item.getTitle());
        holder.id.setText(item.getId().toString());

        Glide.with(context).load("https://image.tmdb.org/t/p/w300_and_h450_bestv2" + item.getPosterPath())
                .placeholder(R.mipmap.ic_launcher_round)
                .into(holder.iv_photo);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public  static class MovieViewHolder extends RecyclerView.ViewHolder {
        LinearLayout moviesLayout;
        TextView movieTitle;
        TextView id;
        ImageView iv_photo;


        public MovieViewHolder(@NonNull View v) {
            super(v);
            moviesLayout = (LinearLayout) v.findViewById(R.id.movies_layout);
            movieTitle = (TextView) v.findViewById(R.id.title);
            id=(TextView)v.findViewById(R.id.idMv);
            iv_photo=(ImageView)v.findViewById(R.id.iv_photo);
        }
    }
    public MoviesAdapter(List<movie> movies, int rowLayout, Context context) {
        this.movies = movies;
        this.rowLayout = rowLayout;
        this.context = context;
    }



}
